1
00:00:00,240 --> 00:00:03,840
Now, while integer overflows and

2
00:00:01,760 --> 00:00:06,160
underflows are the big hitter issue that

3
00:00:03,840 --> 00:00:08,480
causes the most trouble, there are a

4
00:00:06,160 --> 00:00:10,559
bunch of other related integer issues

5
00:00:08,480 --> 00:00:11,840
that can also cause vulnerabilities to

6
00:00:10,559 --> 00:00:14,639
be exploitable.

7
00:00:11,840 --> 00:00:16,880
So, we're going to cover the incorrect

8
00:00:14,639 --> 00:00:18,080
sanity checks or insanity checks, as I

9
00:00:16,880 --> 00:00:20,800
like to call them,

10
00:00:18,080 --> 00:00:23,279
sign or unsigned integer truncation, and

11
00:00:20,800 --> 00:00:26,480
assigned integer extension.

12
00:00:23,279 --> 00:00:29,359
So, first. Insanity checks. What are those?

13
00:00:26,480 --> 00:00:31,039
Well, 50 of the time it works 100 of the

14
00:00:29,359 --> 00:00:33,920
time. That's what developers say when

15
00:00:31,039 --> 00:00:35,920
they're using signed values to do sanity

16
00:00:33,920 --> 00:00:38,160
checks... because 50 of the values are

17
00:00:35,920 --> 00:00:39,840
positive, so, it'll work, right?

18
00:00:38,160 --> 00:00:41,680
So, let's go back to our original stack

19
00:00:39,840 --> 00:00:43,760
buffer overflow type vulnerability.

20
00:00:41,680 --> 00:00:46,160
We've got attacker controlled input here being

21
00:00:43,760 --> 00:00:48,960
passed into "size" and then now, because

22
00:00:46,160 --> 00:00:51,120
we're so much older and wiser, we're

23
00:00:48,960 --> 00:00:53,199
going to add a sanity check: if "size" is

24
00:00:51,120 --> 00:00:55,520
greater than 100, the size of the buffer,

25
00:00:53,199 --> 00:00:56,480
"Nice try, *ATTACKER*! But I'm too clever for

26
00:00:55,520 --> 00:00:59,199
you!!!".

27
00:00:56,480 --> 00:01:02,559
There you go. Well, unfortunately, no!

28
00:00:59,199 --> 00:01:04,080
This is a signed "size" and that means this is

29
00:01:02,559 --> 00:01:05,760
an insanity check!

30
00:01:04,080 --> 00:01:07,439
That sanity check is not actually going

31
00:01:05,760 --> 00:01:09,040
to do anything because an attacker can

32
00:01:07,439 --> 00:01:10,720
cause this to be a value that is

33
00:01:09,040 --> 00:01:12,720
negative, and the negative value will

34
00:01:10,720 --> 00:01:14,640
never be greater than 100. And,

35
00:01:12,720 --> 00:01:16,880
consequently, it will not be caught and

36
00:01:14,640 --> 00:01:19,600
it will proceed on into this attacker

37
00:01:16,880 --> 00:01:21,840
controlled "memcpy". The result,

38
00:01:19,600 --> 00:01:24,960
if we ran it like this "0x10" and

39
00:01:21,840 --> 00:01:27,119
"OST2_rules!" great, it works! And, oh, we put

40
00:01:24,960 --> 00:01:28,320
"0x100" in and... "Nice try, *ATTACKER*! But I'm too

41
00:01:27,119 --> 00:01:30,400
clever for you!!!".

42
00:01:28,320 --> 00:01:31,600
Thumbs up and success! And, we're finished,

43
00:01:30,400 --> 00:01:34,960
right?

44
00:01:31,600 --> 00:01:37,119
Well, no! Because if an attacker passed in

45
00:01:34,960 --> 00:01:39,280
"0x80000000", then it

46
00:01:37,119 --> 00:01:42,079
successfully bypassed the sanity check,

47
00:01:39,280 --> 00:01:44,640
or insanity check, and lead to buffer

48
00:01:42,079 --> 00:01:46,000
overflow. So, this is what you should be

49
00:01:44,640 --> 00:01:48,079
like when you're a developer and your

50
00:01:46,000 --> 00:01:49,759
sanity check fails! You should be

51
00:01:48,079 --> 00:01:52,960
questioning your existence and you

52
00:01:49,759 --> 00:01:54,560
should be saying: "nooooooo!!! why?!?!?!"

53
00:01:52,960 --> 00:01:56,000
And this is what you should be like, when

54
00:01:54,560 --> 00:01:58,719
you're an attacker and their sanity

55
00:01:56,000 --> 00:02:00,560
check fails: "super happy"!

56
00:01:58,719 --> 00:02:02,960
So, now let's just look at a few more

57
00:02:00,560 --> 00:02:04,399
quick examples of that. Same idea,

58
00:02:02,960 --> 00:02:05,600
you know, this was our more complicated

59
00:02:04,399 --> 00:02:08,399
version from before. We've got our

60
00:02:05,600 --> 00:02:09,920
attacker controlled value coming in.

61
00:02:08,399 --> 00:02:13,520
"header.size" is attacker controlled and we're

62
00:02:09,920 --> 00:02:15,840
checking it for "0x1000", right? Same idea,

63
00:02:13,520 --> 00:02:18,800
"size" is once again signed and

64
00:02:15,840 --> 00:02:19,680
consequently this is an insanity check.

65
00:02:18,800 --> 00:02:23,280
So,

66
00:02:19,680 --> 00:02:26,319
"BOOM"! It's all good for low values, but

67
00:02:23,280 --> 00:02:29,040
"0x2000", sure! Looks like it's working, but if

68
00:02:26,319 --> 00:02:30,879
the attacker provides all "f"s, then they

69
00:02:29,040 --> 00:02:33,920
will successfully bypass the sanity

70
00:02:30,879 --> 00:02:36,160
check, insanity check, and they will

71
00:02:33,920 --> 00:02:38,640
successfully integer overflow and lead

72
00:02:36,160 --> 00:02:41,440
to an "under allocation", "over-copy"

73
00:02:38,640 --> 00:02:45,040
and, again, developer says: "whyyyy?!?!?!?!"

74
00:02:41,440 --> 00:02:47,680
And the attacker says: "so happy!"

75
00:02:45,040 --> 00:02:50,000
So, again, same idea, well, you know, this

76
00:02:47,680 --> 00:02:52,000
code could be improved. Let's check for

77
00:02:50,000 --> 00:02:54,160
the, you know, integer overflow itself,

78
00:02:52,000 --> 00:02:56,000
right? Let's, let's do this! Let's see if

79
00:02:54,160 --> 00:02:57,680
that's going to be too big for this,

80
00:02:56,000 --> 00:03:00,080
you know, 32-bit value that it could

81
00:02:57,680 --> 00:03:01,760
potentially store. Well, no! All you've

82
00:03:00,080 --> 00:03:04,879
done there is you've given yourself an

83
00:03:01,760 --> 00:03:07,120
insanity check plus ACID math, right?

84
00:03:04,879 --> 00:03:08,879
So, the ACID math will, once again, overflow

85
00:03:07,120 --> 00:03:11,200
right here, before you ever even get a

86
00:03:08,879 --> 00:03:13,680
chance to do this sanity check. And, of

87
00:03:11,200 --> 00:03:15,840
course, once more... "Why?!"

88
00:03:13,680 --> 00:03:18,720
"Whyyyyyyyyy?!?!?!"

89
00:03:15,840 --> 00:03:21,760
And the attacker is like: "Because I'm

90
00:03:18,720 --> 00:03:25,360
happy, to cap along if you feel like a

91
00:03:21,760 --> 00:03:27,920
room without a roof. Because I'm happy..."

92
00:03:25,360 --> 00:03:29,840
and so forth. Now, see, I've told other

93
00:03:27,920 --> 00:03:31,599
OST2 instructors that they can sing in

94
00:03:29,840 --> 00:03:33,360
their class if they want to,

95
00:03:31,599 --> 00:03:35,519
but, so far, no one has actually taken me

96
00:03:33,360 --> 00:03:36,640
up in that. But really, you know, here's

97
00:03:35,519 --> 00:03:38,400
the thing you should be thinking if

98
00:03:36,640 --> 00:03:40,000
you're a developer. You see how happy

99
00:03:38,400 --> 00:03:42,400
that makes the attackers when your

100
00:03:40,000 --> 00:03:44,640
sanity check fails, right? How much joy

101
00:03:42,400 --> 00:03:46,720
that brings them! Don't you want to take

102
00:03:44,640 --> 00:03:49,840
that joy from them? Don't you want to

103
00:03:46,720 --> 00:03:52,159
steal that happiness from the attackers?

104
00:03:49,840 --> 00:03:54,400
You should! You should want to do that,

105
00:03:52,159 --> 00:03:55,680
by doing the appropriate sanity checks!

106
00:03:54,400 --> 00:03:57,280
Now, at this point the attackers are probably

107
00:03:55,680 --> 00:03:59,680
thinking: "Hey, wait a second! Whose side

108
00:03:57,280 --> 00:04:01,280
are you on?" I'm on the defender's side,

109
00:03:59,680 --> 00:04:02,959
but I'm still going to teach you how to

110
00:04:01,280 --> 00:04:04,959
find vulnerabilities, because you're a

111
00:04:02,959 --> 00:04:06,319
necessary component to a healthy immune

112
00:04:04,959 --> 00:04:09,200
system.

113
00:04:06,319 --> 00:04:11,120
Moving on. Integer truncation.

114
00:04:09,200 --> 00:04:13,040
Well, you know, it's probably fine, right?

115
00:04:11,120 --> 00:04:15,519
If I just take a large value and

116
00:04:13,040 --> 00:04:17,680
truncate it down to a smaller value.

117
00:04:15,519 --> 00:04:19,199
Here's an example where we have our

118
00:04:17,680 --> 00:04:20,720
unsigned int.

119
00:04:19,199 --> 00:04:22,560
Oh! It's unsigned! We're not going to have

120
00:04:20,720 --> 00:04:25,919
any signedness issues!

121
00:04:22,560 --> 00:04:29,360
And we have a "unsigned short

122
00:04:25,919 --> 00:04:31,120
alloc_size;" and then, essentially, we

123
00:04:29,360 --> 00:04:34,320
have this opportunity for an integer

124
00:04:31,120 --> 00:04:36,320
overflow and opportunity for truncation.

125
00:04:34,320 --> 00:04:38,880
It's ultimately going to be ACID math

126
00:04:36,320 --> 00:04:41,520
plus truncation, as a 32-bit value from

127
00:04:38,880 --> 00:04:44,240
the unsigned "header.size" gets truncated down to

128
00:04:41,520 --> 00:04:46,080
a 16-bit value for the "alloc_size",

129
00:04:44,240 --> 00:04:48,960
which will lead to our classical

130
00:04:46,080 --> 00:04:50,960
"under allocation", "over-copy".

131
00:04:48,960 --> 00:04:53,440
That result, you know, you can pass in

132
00:04:50,960 --> 00:04:55,840
something like "0x100" and it's all good,

133
00:04:53,440 --> 00:04:56,800
but if you pass in something like

134
00:04:55,840 --> 00:04:58,720
"0x10000",

135
00:04:56,800 --> 00:05:00,639
then that will ultimately exceed the

136
00:04:58,720 --> 00:05:02,720
available 16-bit

137
00:05:00,639 --> 00:05:06,320
bounds and that will, again, lead to an

138
00:05:02,720 --> 00:05:08,320
"under allocation" and "over-copy".

139
00:05:06,320 --> 00:05:10,960
Sign extension. And this is a heart

140
00:05:08,320 --> 00:05:13,520
getting bigger by the way, not smaller.

141
00:05:10,960 --> 00:05:14,720
"Sign Extension" would be situations where,

142
00:05:13,520 --> 00:05:17,039
for instance, you're doing point

143
00:05:14,720 --> 00:05:19,280
arithmetic on signed values and those

144
00:05:17,039 --> 00:05:20,880
small little 16-bit sign values, that you

145
00:05:19,280 --> 00:05:23,759
don't worry about, can all of a sudden

146
00:05:20,880 --> 00:05:25,919
become big 64-bit negative values that

147
00:05:23,759 --> 00:05:28,160
you, absolutely, should worry about!

148
00:05:25,919 --> 00:05:29,840
So, here is some new simple trivial code.

149
00:05:28,160 --> 00:05:32,400
We've got a buffer. We've got "ptr1",

150
00:05:29,840 --> 00:05:34,800
"ptr2", "short size1 = 0x8000;",

151
00:05:32,400 --> 00:05:36,800
"int size2 = 0x80000000;", and

152
00:05:34,800 --> 00:05:39,199
"ptr1" is going to be "buf" plus

153
00:05:36,800 --> 00:05:41,039
"size1". So, all right. So, it's "buf" plus

154
00:05:39,199 --> 00:05:43,600
"0x8000". Well, that's clearly going to be an

155
00:05:41,039 --> 00:05:46,320
out of bounds type of thing, but that's

156
00:05:43,600 --> 00:05:48,400
not the point right now. "buf" plus "0x8000"

157
00:05:46,320 --> 00:05:51,440
and "buf" plus

158
00:05:48,400 --> 00:05:53,360
"0x80000000". All right? So, what are

159
00:05:51,440 --> 00:05:56,400
the values going to be when we print all

160
00:05:53,360 --> 00:05:59,360
of this out? Well, these, unfortunately, are

161
00:05:56,400 --> 00:06:01,600
signed shorts and signed ints, and that

162
00:05:59,360 --> 00:06:04,639
means that sign extension is going to be

163
00:06:01,600 --> 00:06:07,840
in play. So, this "0x8000" is not going to be

164
00:06:04,639 --> 00:06:10,319
"buf" plus "0x8000". It's going to be negative

165
00:06:07,840 --> 00:06:12,720
"0x8000", and that's going to be "buf" minus

166
00:06:10,319 --> 00:06:15,520
"0x8000". So, your pointer is actually going

167
00:06:12,720 --> 00:06:17,360
to point before the buffer.

168
00:06:15,520 --> 00:06:20,400
So, something like this. If we looked at

169
00:06:17,360 --> 00:06:22,080
the buffer address, would be

170
00:06:20,400 --> 00:06:23,520
"0x7ffcc42...", is going to be randomized

171
00:06:22,080 --> 00:06:26,160
if you, you know, run this on your own

172
00:06:23,520 --> 00:06:29,440
system, but the relevant point is that it

173
00:06:26,160 --> 00:06:32,319
started out at "42a...", and then

174
00:06:29,440 --> 00:06:33,759
when you "added" "0x8000", which was

175
00:06:32,319 --> 00:06:37,120
a signed value, which was actually

176
00:06:33,759 --> 00:06:38,720
negative "0x8000", then you start at

177
00:06:37,120 --> 00:06:40,560
"429...". So, you've actually going to a

178
00:06:38,720 --> 00:06:42,080
lower address, which is outside of the

179
00:06:40,560 --> 00:06:44,800
bounds. So, you went backwards instead of

180
00:06:42,080 --> 00:06:47,360
forwards. Same problem with the 32-bit

181
00:06:44,800 --> 00:06:49,520
version or, yeah, the 32-bit version it,

182
00:06:47,360 --> 00:06:53,199
again, subtracted "0x80000000"

183
00:06:49,520 --> 00:06:54,800
is a 64-bit address and,

184
00:06:53,199 --> 00:06:56,960
consequently, you went backwards in

185
00:06:54,800 --> 00:06:59,759
memory. So, the overall point of this

186
00:06:56,960 --> 00:07:01,599
section is that almost all the time you

187
00:06:59,759 --> 00:07:03,039
probably don't need to be using signed

188
00:07:01,599 --> 00:07:05,039
integers. Yes, if you're doing, you know,

189
00:07:03,039 --> 00:07:07,280
some funky math for, you know, you're

190
00:07:05,039 --> 00:07:09,039
doing a math library, maybe in some cases

191
00:07:07,280 --> 00:07:10,800
you're doing, you know,

192
00:07:09,039 --> 00:07:12,639
"(x, y, z)" cartesian coordinates, something like

193
00:07:10,800 --> 00:07:15,199
that, sure, then you need negative numbers,

194
00:07:12,639 --> 00:07:16,880
but like I would say like 99% of

195
00:07:15,199 --> 00:07:19,199
the time, the reason someone uses a

196
00:07:16,880 --> 00:07:21,599
"signed int" instead of an "unsigned int"

197
00:07:19,199 --> 00:07:23,680
is just because it's shorter to type, right?

198
00:07:21,599 --> 00:07:26,160
Right??? You're guilty of it! I'm guilty of it!

199
00:07:23,680 --> 00:07:28,080
Everybody's guilty of it! It's just

200
00:07:26,160 --> 00:07:28,880
easier to type into an "unsigned int",

201
00:07:28,080 --> 00:07:30,560
right?

202
00:07:28,880 --> 00:07:32,560
Well, you know, I agree that that's too

203
00:07:30,560 --> 00:07:34,720
much work to type "unsigned int". So, that's

204
00:07:32,560 --> 00:07:37,759
what "typedef"s were invented for. You can,

205
00:07:34,720 --> 00:07:39,840
you know, change "unsigned int" to "uint" or

206
00:07:37,759 --> 00:07:41,919
something like that or "ui". I don't know.

207
00:07:39,840 --> 00:07:44,160
I don't care! Just don't use signed

208
00:07:41,919 --> 00:07:46,000
integers! If I were to make this an even

209
00:07:44,160 --> 00:07:48,080
stronger statement, I would say: "Death to

210
00:07:46,000 --> 00:07:50,240
signed integers!" I would say: "Sign Sizes

211
00:07:48,080 --> 00:07:52,479
are inSane!" And... "Signed Lengths should

212
00:07:50,240 --> 00:07:55,360
Leave!" "Signed Offsets are Out!" and...

213
00:07:52,479 --> 00:07:57,919
"Sign Counts can take a hike"!, right? So, these

214
00:07:55,360 --> 00:08:01,120
sort of things that you find in the code...

215
00:07:57,919 --> 00:08:03,199
"size"s, "length"s, "offset"s and "count"s, well,

216
00:08:01,120 --> 00:08:05,759
whenever I see those kind of things

217
00:08:03,199 --> 00:08:06,720
"signed sizes" should signal your

218
00:08:05,759 --> 00:08:08,720
sploity sense!

219
00:08:06,720 --> 00:08:10,319
They certainly signal my sploity sense.

220
00:08:08,720 --> 00:08:12,560
Basically, if I look through some code

221
00:08:10,319 --> 00:08:14,560
and I see a whole bunch of "int"s being

222
00:08:12,560 --> 00:08:17,520
used, and I see them being named like

223
00:08:14,560 --> 00:08:19,599
"size", "length", etc, then I know that this is

224
00:08:17,520 --> 00:08:21,919
not hardened code and, you know, I start

225
00:08:19,599 --> 00:08:23,520
thinking, "oh, goody goody", because this

226
00:08:21,919 --> 00:08:25,440
means this is a developer who isn't

227
00:08:23,520 --> 00:08:28,639
aware of these kind of problems that can

228
00:08:25,440 --> 00:08:29,840
occur both for integer overflows or for

229
00:08:28,639 --> 00:08:32,479
these

230
00:08:29,840 --> 00:08:35,039
other integer issues.

231
00:08:32,479 --> 00:08:36,800
And if I took it just one step further,

232
00:08:35,039 --> 00:08:40,479
I would start going for something like

233
00:08:36,800 --> 00:08:42,959
some "fahrenheit 451 dystopian propaganda"

234
00:08:40,479 --> 00:08:46,000
where I say that: "you should lead a sign

235
00:08:42,959 --> 00:08:48,320
size slaughter on your code today!"

236
00:08:46,000 --> 00:08:50,720
All right. Let's add in some 1984 as well

237
00:08:48,320 --> 00:08:53,040
because big brother is watching!

238
00:08:50,720 --> 00:08:55,360
Well, you know, the point is sign "size"s

239
00:08:53,040 --> 00:08:56,959
are bad and you should get rid of them!

240
00:08:55,360 --> 00:08:59,839
Let's go look at some real examples of

241
00:08:56,959 --> 00:08:59,839
why this is true.

