1
00:00:00,320 --> 00:00:04,480
integer overflow and underflow

2
00:00:02,000 --> 00:00:07,200
vulnerabilities. What are they? Well, it's

3
00:00:04,480 --> 00:00:09,519
when signed and unsigned numbers exceed

4
00:00:07,200 --> 00:00:12,719
their available positive or negative

5
00:00:09,519 --> 00:00:15,360
ranges, due to ACID math. You're going to

6
00:00:12,719 --> 00:00:17,680
come to hate ACID math in this class.

7
00:00:15,360 --> 00:00:20,000
So, just a quick refresher of two's

8
00:00:17,680 --> 00:00:21,920
complement numbering system: one's

9
00:00:20,000 --> 00:00:23,760
complement is the notion of you take a

10
00:00:21,920 --> 00:00:26,400
value and you flip all the bits and

11
00:00:23,760 --> 00:00:29,439
two's complement is one's complement...

12
00:00:26,400 --> 00:00:31,920
so, flip all the bits and add one to that.

13
00:00:29,439 --> 00:00:33,680
In C signed types, meaning things that

14
00:00:31,920 --> 00:00:36,239
can take on a positive and negative

15
00:00:33,680 --> 00:00:38,399
value, are represented as two's

16
00:00:36,239 --> 00:00:40,160
complement values, where basically

17
00:00:38,399 --> 00:00:42,480
effectively half the range is available

18
00:00:40,160 --> 00:00:44,399
for positive and half for negative.

19
00:00:42,480 --> 00:00:47,039
So, for instance, if we had an eight bit

20
00:00:44,399 --> 00:00:49,360
value like a char the available values

21
00:00:47,039 --> 00:00:53,440
are, you know, binary eight bits zero zero

22
00:00:49,360 --> 00:00:56,320
zero to all ones hex zero zero to all

23
00:00:53,440 --> 00:00:59,760
f's and unsigned decimal one uh sorry

24
00:00:56,320 --> 00:01:01,760
zero up through 255. So when it's an

25
00:00:59,760 --> 00:01:04,640
unsigned 8-bit number these values are

26
00:01:01,760 --> 00:01:06,720
all positive 0 to 255.

27
00:01:04,640 --> 00:01:09,680
If, on the other hand, it is a signed

28
00:01:06,720 --> 00:01:13,840
8-bit number then you have 0 and it goes

29
00:01:09,680 --> 00:01:17,200
1 to 127 and then it loops around and

30
00:01:13,840 --> 00:01:20,479
all of a sudden becomes negative 128

31
00:01:17,200 --> 00:01:21,680
negative 127, 126, etcetera, down to

32
00:01:20,479 --> 00:01:23,600
negative one.

33
00:01:21,680 --> 00:01:25,280
So, basically, like I said, we have

34
00:01:23,600 --> 00:01:27,040
effectively half the range positive, half

35
00:01:25,280 --> 00:01:29,439
the range negative. You can't exactly be

36
00:01:27,040 --> 00:01:31,600
half because you've got zero here

37
00:01:29,439 --> 00:01:33,920
but uh but, yeah, so, that is kind of the

38
00:01:31,600 --> 00:01:35,520
thing that gets people is this notion of

39
00:01:33,920 --> 00:01:36,799
you just keep increasing the values and

40
00:01:35,520 --> 00:01:39,040
all of a sudden, when you're halfway

41
00:01:36,799 --> 00:01:41,040
through the range, then it immediately

42
00:01:39,040 --> 00:01:42,560
flips over to negative values instead of

43
00:01:41,040 --> 00:01:45,040
positive values.

44
00:01:42,560 --> 00:01:46,640
So, also, I just point out that all the

45
00:01:45,040 --> 00:01:49,520
negative values have the most

46
00:01:46,640 --> 00:01:52,079
significant bit set to 1. So, that is why

47
00:01:49,520 --> 00:01:54,399
that is sometimes called the sign bit

48
00:01:52,079 --> 00:01:56,000
because, basically, in two's complement

49
00:01:54,399 --> 00:01:57,439
system, if the most significant bit is

50
00:01:56,000 --> 00:01:59,200
one, then that's going to be a negative

51
00:01:57,439 --> 00:02:01,680
value and if the most significant bit is

52
00:01:59,200 --> 00:02:03,600
zero, it's a positive value or it's zero.

53
00:02:01,680 --> 00:02:05,600
And, so, the exact same principles apply

54
00:02:03,600 --> 00:02:08,160
if we were looking at 32-bit values or

55
00:02:05,600 --> 00:02:10,879
64-bit values. It's just all zeros, all

56
00:02:08,160 --> 00:02:13,520
zero and one, all zero one zero, etcetera,

57
00:02:10,879 --> 00:02:16,640
zero one, two, three, and then up to

58
00:02:13,520 --> 00:02:18,080
"0x7f...e", "0x7f...f", and that's the last

59
00:02:16,640 --> 00:02:20,959
positive value,

60
00:02:18,080 --> 00:02:23,120
Two billion something, and then it flips

61
00:02:20,959 --> 00:02:25,120
around and becomes negative 2 billion

62
00:02:23,120 --> 00:02:26,720
something, and then negative 2 billion

63
00:02:25,120 --> 00:02:29,680
something minus 1, etcetera, all the way

64
00:02:26,720 --> 00:02:31,920
up to negative 1. Right, so, this is a

65
00:02:29,680 --> 00:02:34,000
thing that, ultimately, causes all sorts

66
00:02:31,920 --> 00:02:36,480
of problems for code,

67
00:02:34,000 --> 00:02:38,239
when things, you know, either a overflow

68
00:02:36,480 --> 00:02:40,239
because they got so high that they got

69
00:02:38,239 --> 00:02:42,400
up to "ff" and then they loop back around

70
00:02:40,239 --> 00:02:44,239
or, b, due to

71
00:02:42,400 --> 00:02:45,920
crossing the boundary between positive

72
00:02:44,239 --> 00:02:48,080
and negative values.

73
00:02:45,920 --> 00:02:50,879
So, like I said, you're going to learn to

74
00:02:48,080 --> 00:02:54,160
fear ACID math in this class because

75
00:02:50,879 --> 00:02:58,080
ACID math is as bad as an ACID bath.

76
00:02:54,160 --> 00:03:00,800
So, here we go ACID math. So, "pot",

77
00:02:58,080 --> 00:03:02,720
addition, subtraction, multiplication and

78
00:03:00,800 --> 00:03:04,400
and shifting to the left, which is

79
00:03:02,720 --> 00:03:06,720
equivalent to multiplication,

80
00:03:04,400 --> 00:03:09,280
all those sort of, uh, arithmetic

81
00:03:06,720 --> 00:03:13,280
operations on ACID, lead to a bad time.

82
00:03:09,280 --> 00:03:15,200
ACID math, ACID bath, and you're dead.

83
00:03:13,280 --> 00:03:18,080
And, if that weren't enough, let us recall

84
00:03:15,200 --> 00:03:20,720
the travails of "alien vs predator" to

85
00:03:18,080 --> 00:03:22,959
remind us that an ACID bath is not a

86
00:03:20,720 --> 00:03:25,120
good time. So, integer overflows: how

87
00:03:22,959 --> 00:03:27,280
exactly does this manifest and why is it

88
00:03:25,120 --> 00:03:29,440
a security problem? Well, let's say we

89
00:03:27,280 --> 00:03:32,080
have some stupid trivial code like this

90
00:03:29,440 --> 00:03:34,959
"unsigned char i = 0;" and then an

91
00:03:32,080 --> 00:03:37,440
infinite loop doing "i++". So, "i"

92
00:03:34,959 --> 00:03:39,280
starts at zero, "++" it's one,

93
00:03:37,440 --> 00:03:43,440
"++" it's two, and it just keeps going,

94
00:03:39,280 --> 00:03:46,799
until it eventually gets to 254, 255 and

95
00:03:43,440 --> 00:03:49,840
then, if you do this math, all ones plus

96
00:03:46,799 --> 00:03:53,760
one, well, the result is one and eight

97
00:03:49,840 --> 00:03:56,159
zeros, but you've only got eight bits to

98
00:03:53,760 --> 00:03:58,480
use for storage of this value, so, this

99
00:03:56,159 --> 00:04:00,799
most significant one just gets dropped,

100
00:03:58,480 --> 00:04:02,799
truncated, and then, effectively, it

101
00:04:00,799 --> 00:04:05,360
becomes all zeros and it wraps back

102
00:04:02,799 --> 00:04:08,239
around from the maximum positive number

103
00:04:05,360 --> 00:04:10,400
down to zero, and "BOOM", that was an

104
00:04:08,239 --> 00:04:12,720
integer overflow!

105
00:04:10,400 --> 00:04:14,640
Then you have signed values. So, you can

106
00:04:12,720 --> 00:04:16,320
start with zero and move your way

107
00:04:14,640 --> 00:04:18,400
through the available values until you

108
00:04:16,320 --> 00:04:21,600
get up to 127 and, when you cross that

109
00:04:18,400 --> 00:04:24,000
boundary into negative 128, "BOOM"! That's

110
00:04:21,600 --> 00:04:25,840
an integer overflow! And then you just

111
00:04:24,000 --> 00:04:28,000
keep adding and adding and adding and

112
00:04:25,840 --> 00:04:31,120
you're at negative one and then you add

113
00:04:28,000 --> 00:04:33,120
plus one and you're back up to zero.

114
00:04:31,120 --> 00:04:34,880
"BOOM"! That's an integer overflow! Now, I'm just

115
00:04:33,120 --> 00:04:36,080
making a big deal about those integer

116
00:04:34,880 --> 00:04:38,400
overflows because that's going to go

117
00:04:36,080 --> 00:04:40,880
into the trailer. So, I was searching for

118
00:04:38,400 --> 00:04:42,880
some, you know, elementary school notion

119
00:04:40,880 --> 00:04:45,520
of under and over and I came with this

120
00:04:42,880 --> 00:04:47,280
picture liked it a lot because, basically,

121
00:04:45,520 --> 00:04:49,680
integer overflows will manifest

122
00:04:47,280 --> 00:04:53,600
themselves as vulnerabilities due to

123
00:04:49,680 --> 00:04:55,840
"under allocation" and "over-copy"ing.

124
00:04:53,600 --> 00:04:58,320
So, found this picture liked it, especially

125
00:04:55,840 --> 00:05:00,720
because the picture reminds you that you

126
00:04:58,320 --> 00:05:02,320
need to "listen to your teacher" and

127
00:05:00,720 --> 00:05:03,759
"look at the pictures". That's what I want out

128
00:05:02,320 --> 00:05:05,520
of you in this class: "listen to the

129
00:05:03,759 --> 00:05:08,240
teacher" and "look at the pictures".

130
00:05:05,520 --> 00:05:10,639
So, for instance, if we have a big number

131
00:05:08,240 --> 00:05:13,039
plus a constant, two big numbers added

132
00:05:10,639 --> 00:05:15,919
together, big number times a constant,

133
00:05:13,039 --> 00:05:19,440
well, that is ACID math an ACID math is

134
00:05:15,919 --> 00:05:21,360
as bad as an ACID bath. So, this integer

135
00:05:19,440 --> 00:05:23,360
overflowing, in the context of the

136
00:05:21,360 --> 00:05:25,919
allocation, means that a big number is

137
00:05:23,360 --> 00:05:29,039
going to become a small number. That's

138
00:05:25,919 --> 00:05:31,199
going to lead to "under allocation". And if

139
00:05:29,039 --> 00:05:34,000
the big number is subsequently used for

140
00:05:31,199 --> 00:05:36,160
a "memcpy", without realizing that it had

141
00:05:34,000 --> 00:05:39,360
underflowed the memory allocation, then

142
00:05:36,160 --> 00:05:41,919
that is an "over-copy"... "under allocation",

143
00:05:39,360 --> 00:05:45,199
"over-copy"... and this tends to happen quite

144
00:05:41,919 --> 00:05:47,199
a bit in code, all thanks to ACID math.

145
00:05:45,199 --> 00:05:48,800
So, this time the trivial example is not so

146
00:05:47,199 --> 00:05:50,400
trivial, just because, you know, I wanted

147
00:05:48,800 --> 00:05:52,240
to show a little bit more of like,

148
00:05:50,400 --> 00:05:54,240
you know, how this might manifest itself in

149
00:05:52,240 --> 00:05:56,960
the real world. So, we've got our attacker

150
00:05:54,240 --> 00:05:58,880
controlled inputs of "argv". We've got,

151
00:05:56,960 --> 00:06:00,960
you know, some notion of like a header that

152
00:05:58,880 --> 00:06:02,639
goes on a data structure and the header

153
00:06:00,960 --> 00:06:04,080
has some, you know, "magic" number in it

154
00:06:02,639 --> 00:06:06,240
that says like this is what type of

155
00:06:04,080 --> 00:06:07,520
header I am and it's got a "size" of

156
00:06:06,240 --> 00:06:09,440
saying, you know, here's the data that I

157
00:06:07,520 --> 00:06:10,560
expect to have after "my_header". So, you

158
00:06:09,440 --> 00:06:12,639
would expect, you know, some sort of

159
00:06:10,560 --> 00:06:14,319
buffer allocation tax the header at the

160
00:06:12,639 --> 00:06:15,440
beginning and then puts however much

161
00:06:14,319 --> 00:06:18,400
size

162
00:06:15,440 --> 00:06:20,160
after that header. So, okay. What happens

163
00:06:18,400 --> 00:06:21,840
here? Well, we've got, you know, a constant

164
00:06:20,160 --> 00:06:24,319
number going to "magic". That's fine. That's

165
00:06:21,840 --> 00:06:27,039
not attacker controlled. We've got

166
00:06:24,319 --> 00:06:28,720
"argv[1]" going into "strtoul".

167
00:06:27,039 --> 00:06:30,639
Well, that is attacker controlled and

168
00:06:28,720 --> 00:06:32,000
that means attacker controlled value is

169
00:06:30,639 --> 00:06:34,479
going to come out of that and catch

170
00:06:32,000 --> 00:06:36,560
"header.size" on fire!

171
00:06:34,479 --> 00:06:38,560
Then, we have this hard-coded

172
00:06:36,560 --> 00:06:41,919
"sizeof(my_header_t)", plus the attacker controlled

173
00:06:38,560 --> 00:06:44,240
value and... what is that? That is ACID math!

174
00:06:41,919 --> 00:06:46,319
And that's as bad as an ACID bath!

175
00:06:44,240 --> 00:06:47,840
All right, so, ACID math here if the

176
00:06:46,319 --> 00:06:49,919
attacker could cause this to be an

177
00:06:47,840 --> 00:06:52,479
extremely large value, right? What is

178
00:06:49,919 --> 00:06:54,319
"header.size"? It's an "unsigned long int".

179
00:06:52,479 --> 00:06:57,039
So, if this is an extremely large value

180
00:06:54,319 --> 00:06:58,960
coming in here, then that plus

181
00:06:57,039 --> 00:07:00,160
"header.size", whatever "header.size" is, let's say,

182
00:06:58,960 --> 00:07:03,039
it's eight,

183
00:07:00,160 --> 00:07:05,440
if "header.size" plus that overflows,

184
00:07:03,039 --> 00:07:07,599
integer overflows, then "alloc_size" will

185
00:07:05,440 --> 00:07:08,479
be a small value instead of a large

186
00:07:07,599 --> 00:07:11,440
value.

187
00:07:08,479 --> 00:07:14,720
Then that will "boom", "boom", "boom"

188
00:07:11,440 --> 00:07:17,280
be handed into "malloc". So, "alloc_size"

189
00:07:14,720 --> 00:07:19,039
could be small instead of big even

190
00:07:17,280 --> 00:07:20,720
though the intention is that this

191
00:07:19,039 --> 00:07:23,199
"header.size" is big like there should be a big

192
00:07:20,720 --> 00:07:25,039
amount of data after the end of the,

193
00:07:23,199 --> 00:07:27,360
you know, header on this allocation, but,

194
00:07:25,039 --> 00:07:29,840
unfortunately, it's "under allocated" and

195
00:07:27,360 --> 00:07:31,840
we'll see later if there's an "over-copy"

196
00:07:29,840 --> 00:07:33,599
but allocation small

197
00:07:31,840 --> 00:07:35,360
allocates this buffer and this is going

198
00:07:33,599 --> 00:07:37,199
to be explicitly an "under allocation",

199
00:07:35,360 --> 00:07:39,919
which I'm going to show this way with a

200
00:07:37,199 --> 00:07:42,720
little bit of like a an under like push

201
00:07:39,919 --> 00:07:44,400
down shading whatever.

202
00:07:42,720 --> 00:07:46,639
All right, so, the under allocated buffer

203
00:07:44,400 --> 00:07:48,639
check if it's "NULL". No, it's not. All right,

204
00:07:46,639 --> 00:07:50,960
and then print out the pointer of

205
00:07:48,639 --> 00:07:52,800
address now we've got a "memcpy" and

206
00:07:50,960 --> 00:07:55,039
it's copying "sizeof(my_header)". Well,

207
00:07:52,800 --> 00:07:58,319
that's not an attacker controlled value,

208
00:07:55,039 --> 00:07:59,919
and it's copying from header which is,

209
00:07:58,319 --> 00:08:01,919
you know, semi-attack controlled only in

210
00:07:59,919 --> 00:08:03,840
the sense of, you know, "size" is attacker

211
00:08:01,919 --> 00:08:05,440
controlled and "magic" is not probably

212
00:08:03,840 --> 00:08:07,120
should have drawn that on here but we'll

213
00:08:05,440 --> 00:08:09,440
just remember that for now and that's

214
00:08:07,120 --> 00:08:11,280
going into "buf". Well, that is an

215
00:08:09,440 --> 00:08:13,360
OK "memcopy" that is not buffer overflowing.

216
00:08:11,280 --> 00:08:15,919
So, it's only just copying eight bytes into

217
00:08:13,360 --> 00:08:15,919
the buffer.

218
00:08:16,160 --> 00:08:19,520
Well, actually I guess it could

219
00:08:17,440 --> 00:08:21,680
technically overflow. Depends on the

220
00:08:19,520 --> 00:08:25,599
values as you'll see later.

221
00:08:21,680 --> 00:08:28,479
All right, so, then the "buf" pointer is

222
00:08:25,599 --> 00:08:30,639
moved forward via the "sizeof" by the

223
00:08:28,479 --> 00:08:32,640
total "sizeof(my_header)".

224
00:08:30,639 --> 00:08:34,479
OK. So, now "buf" is not pointing at the

225
00:08:32,640 --> 00:08:36,640
beginning of the buffer, but some offset

226
00:08:34,479 --> 00:08:38,159
into the buffer. If that were an attacker

227
00:08:36,640 --> 00:08:39,680
controlled value we would be worried

228
00:08:38,159 --> 00:08:41,200
about, you know, potential out of bound

229
00:08:39,680 --> 00:08:42,159
write tight vulnerabilities at this

230
00:08:41,200 --> 00:08:44,000
point.

231
00:08:42,159 --> 00:08:45,760
But then, you know, print out, you know, the

232
00:08:44,000 --> 00:08:47,600
allocation sizes, the attacker controlled

233
00:08:45,760 --> 00:08:49,519
values and now we're going into a

234
00:08:47,600 --> 00:08:52,160
"memcpy" again. Now, we have an attacker

235
00:08:49,519 --> 00:08:54,399
controlled "size" which is a large "size"

236
00:08:52,160 --> 00:08:56,399
presumably if an attacker was causing a

237
00:08:54,399 --> 00:08:59,440
integer overflow at this point large

238
00:08:56,399 --> 00:09:01,920
size copying from a large or from an

239
00:08:59,440 --> 00:09:03,600
attacker controlled buffer which could

240
00:09:01,920 --> 00:09:06,560
be large could be small

241
00:09:03,600 --> 00:09:07,600
into a purposefully "under allocated"

242
00:09:06,560 --> 00:09:09,279
buffer.

243
00:09:07,600 --> 00:09:10,880
Well, that is the kind of thing that

244
00:09:09,279 --> 00:09:12,560
should make your "splitty sense" go off!

245
00:09:10,880 --> 00:09:14,480
You've got fully attack controlled

246
00:09:12,560 --> 00:09:16,800
length, fully attacker controlled data

247
00:09:14,480 --> 00:09:19,200
and, to boot, you've got a intentionally

248
00:09:16,800 --> 00:09:21,440
forced "under allocated" buffer.

249
00:09:19,200 --> 00:09:24,560
So, that's going to cause a problem under

250
00:09:21,440 --> 00:09:26,480
allocation via integer overflow and

251
00:09:24,560 --> 00:09:29,279
"over-copy" and this just becomes, you know, a

252
00:09:26,480 --> 00:09:30,880
typical heap overflow! So, if we run that

253
00:09:29,279 --> 00:09:33,279
we could do, for instance,

254
00:09:30,880 --> 00:09:35,040
"under allocation", "over-copy" if we say one

255
00:09:33,279 --> 00:09:36,399
saying we want to, you know, have a size

256
00:09:35,040 --> 00:09:38,880
of one we're going to copy that

257
00:09:36,399 --> 00:09:41,120
self-reported size one allocation is one

258
00:09:38,880 --> 00:09:43,519
plus the "header.size", which is eight.

259
00:09:41,120 --> 00:09:45,600
So, this is nine buffer points wherever it

260
00:09:43,519 --> 00:09:48,320
points on the heap it's "memcpy"ing one

261
00:09:45,600 --> 00:09:50,320
byte into a buffer of size nine. Great!

262
00:09:48,320 --> 00:09:53,200
No problem and there's the copied string

263
00:09:50,320 --> 00:09:56,800
one byte of "W" from the "What_up?".

264
00:09:53,200 --> 00:09:59,040
Then, here, what if we say it is "0x10",

265
00:09:56,800 --> 00:10:02,240
16 bytes? So, it's going to allocate a total

266
00:09:59,040 --> 00:10:05,519
size of 18 and copying 10 bytes into 18.

267
00:10:02,240 --> 00:10:08,560
Sure! No problem! But, what if the attacker

268
00:10:05,519 --> 00:10:09,760
provides a size of all f's, write

269
00:10:08,560 --> 00:10:12,079
4 billion!

270
00:10:09,760 --> 00:10:15,680
Now, the reported size is 4 billion and

271
00:10:12,079 --> 00:10:16,720
the allocation size is 7. So, I said

272
00:10:15,680 --> 00:10:18,240
before, you know, when I was thinking

273
00:10:16,720 --> 00:10:19,680
about the header, I said... "oh just copying

274
00:10:18,240 --> 00:10:21,839
the header in. Is okay! That's not gonna

275
00:10:19,680 --> 00:10:23,279
overflow!" Well, the header is eight bytes

276
00:10:21,839 --> 00:10:25,360
and we just caused an allocation of

277
00:10:23,279 --> 00:10:27,040
seven. So, yes! Even just the header

278
00:10:25,360 --> 00:10:29,279
allocation, even though, it's not attacker

279
00:10:27,040 --> 00:10:33,040
controlled, in terms of the size of the

280
00:10:29,279 --> 00:10:35,279
header, it still will actually overflow.

281
00:10:33,040 --> 00:10:38,399
And so, ultimately once you get to the

282
00:10:35,279 --> 00:10:40,880
big "memcpy" which is the fff bytes into

283
00:10:38,399 --> 00:10:43,519
the buffer that of course is going to

284
00:10:40,880 --> 00:10:45,040
completely smash the heap. Now, just as an

285
00:10:43,519 --> 00:10:46,399
aside, while I was playing with this,

286
00:10:45,040 --> 00:10:48,959
you know, I tried, you know, some smaller

287
00:10:46,399 --> 00:10:50,800
values like "0x1000" at one point and

288
00:10:48,959 --> 00:10:52,720
that would actually crash as well. And so

289
00:10:50,800 --> 00:10:54,560
I said: "copying a thousand bytes into a

290
00:10:52,720 --> 00:10:56,720
size of "0x1008", why is that a problem?"

291
00:10:54,560 --> 00:10:58,800
There's no integer overflow here

292
00:10:56,720 --> 00:11:01,040
but, you know, if you look at your memory

293
00:10:58,800 --> 00:11:02,880
layout and mind your memory map

294
00:11:01,040 --> 00:11:04,800
you'll see you're copying from "argv[1]",

295
00:11:02,880 --> 00:11:06,560
that is pretty close, generally, to the

296
00:11:04,800 --> 00:11:09,680
bottom of the stack, which means it's

297
00:11:06,560 --> 00:11:11,680
just going to copy up, and ultimately run

298
00:11:09,680 --> 00:11:13,519
off the bounds of the stack and hit

299
00:11:11,680 --> 00:11:15,440
unmapped memory location. So, it's

300
00:11:13,519 --> 00:11:17,519
actually unfortunately common that

301
00:11:15,440 --> 00:11:20,320
allocators may not be checking for

302
00:11:17,519 --> 00:11:22,240
integer overflows themselves. So, even if

303
00:11:20,320 --> 00:11:24,000
the bug was not in your code your

304
00:11:22,240 --> 00:11:26,240
allocator might be causing an integer

305
00:11:24,000 --> 00:11:28,959
overflow. So, for instance, normally when

306
00:11:26,240 --> 00:11:31,600
you ask for "X" bytes from an allocator

307
00:11:28,959 --> 00:11:34,399
behind the scenes, it may allocate

308
00:11:31,600 --> 00:11:35,120
"X+Y" bytes in order to create some space

309
00:11:34,399 --> 00:11:38,000
for

310
00:11:35,120 --> 00:11:40,959
heap metadata that the allocator manages

311
00:11:38,000 --> 00:11:42,880
itself. So, this "X+Y" allocation could

312
00:11:40,959 --> 00:11:44,240
actually overflow and ultimately the

313
00:11:42,880 --> 00:11:46,240
allocator could

314
00:11:44,240 --> 00:11:49,279
allocate too little space.

315
00:11:46,240 --> 00:11:51,440
So, Microsoft's Azure IOT folks, internet

316
00:11:49,279 --> 00:11:53,680
of things folks, had

317
00:11:51,440 --> 00:11:55,279
been looking at a bunch of IOT libraries

318
00:11:53,680 --> 00:11:57,040
and they found like a whole bunch of

319
00:11:55,279 --> 00:11:59,760
these things were having integer

320
00:11:57,040 --> 00:12:01,760
overflows inside the allocation library

321
00:11:59,760 --> 00:12:03,600
itself. So, they dubbed it

322
00:12:01,760 --> 00:12:05,279
"BadAlloc" bugs

323
00:12:03,600 --> 00:12:06,560
and, you know, they they pointed it out in

324
00:12:05,279 --> 00:12:09,120
a whole bunch of things and led a big

325
00:12:06,560 --> 00:12:10,320
massive coordinated disclosure campaign.

326
00:12:09,120 --> 00:12:12,079
But, you know, I've seen these sort of

327
00:12:10,320 --> 00:12:14,079
things, a whole bunch in code I've looked

328
00:12:12,079 --> 00:12:15,360
at as well that had nothing to do with

329
00:12:14,079 --> 00:12:17,360
this code that, you know, I'll show you a

330
00:12:15,360 --> 00:12:19,360
picture of in a second, so, you really

331
00:12:17,360 --> 00:12:22,000
have to look in your code and say...

332
00:12:19,360 --> 00:12:24,160
OK! Now, I'm aware of integer overflows

333
00:12:22,000 --> 00:12:25,440
is my heap allocator gonna overflow on

334
00:12:24,160 --> 00:12:27,200
me.

335
00:12:25,440 --> 00:12:29,279
So from their BlackHat talk from the

336
00:12:27,200 --> 00:12:32,079
Azure folks uh, you know, you can imagine

337
00:12:29,279 --> 00:12:36,320
again, you know, you ask for "X", "X" is 1024

338
00:12:32,079 --> 00:12:39,279
malloc allocates eight plus 1024 in

339
00:12:36,320 --> 00:12:43,120
order to create some space for metadata,

340
00:12:39,279 --> 00:12:45,360
but that 8 plus 1024 is ACID math, and

341
00:12:43,120 --> 00:12:47,920
it's as bad as an ACID bath. So, that

342
00:12:45,360 --> 00:12:49,839
could actually overflow because if the

343
00:12:47,920 --> 00:12:51,120
amount that was being asked for is again

344
00:12:49,839 --> 00:12:54,720
close to that

345
00:12:51,120 --> 00:12:57,839
maximum range the 4 billion. Then 8 plus

346
00:12:54,720 --> 00:13:00,240
4 billion is 7, just like we saw with the

347
00:12:57,839 --> 00:13:02,240
"under allocation", "over-copy".

348
00:13:00,240 --> 00:13:04,480
Similarly, you can have, you know, things

349
00:13:02,240 --> 00:13:06,079
where if the value is, you know,

350
00:13:04,480 --> 00:13:07,680
essentially half of the range and a

351
00:13:06,079 --> 00:13:09,839
little one, one more than half of the

352
00:13:07,680 --> 00:13:11,920
range, then if you have something like

353
00:13:09,839 --> 00:13:14,000
"calloc", which will do a multiplication

354
00:13:11,920 --> 00:13:16,000
behind the scenes, that multiplication

355
00:13:14,000 --> 00:13:17,839
can overflow the available range and

356
00:13:16,000 --> 00:13:20,160
lead to an "under allocation" and that

357
00:13:17,839 --> 00:13:22,079
multiplication is ACID math, which is as

358
00:13:20,160 --> 00:13:23,600
bad as an ACID bath.

359
00:13:22,079 --> 00:13:24,959
All right. So, here's the example that

360
00:13:23,600 --> 00:13:28,000
they, you know, drilled down on a little

361
00:13:24,959 --> 00:13:31,120
bit. It's from "Free RTOS". So, they had like

362
00:13:28,000 --> 00:13:33,519
the "pvPortMalloc" which had the

363
00:13:31,120 --> 00:13:35,200
"xWantedSize" is the attack controls that

364
00:13:33,519 --> 00:13:37,120
are value that, you know, is being asked

365
00:13:35,200 --> 00:13:39,519
for as part of the allocation

366
00:13:37,120 --> 00:13:42,320
that value is checked to see if it's

367
00:13:39,519 --> 00:13:44,800
greater than zero and if so then they

368
00:13:42,320 --> 00:13:48,399
take that value and they add

369
00:13:44,800 --> 00:13:50,800
"heapSTRUCT_SIZE" to it. That is ACID math!

370
00:13:48,399 --> 00:13:52,240
And furthermore, it just keeps going and

371
00:13:50,800 --> 00:13:54,639
there's another, you know, check for

372
00:13:52,240 --> 00:13:56,399
alignment and stuff and if that's found

373
00:13:54,639 --> 00:13:59,040
then ultimately there's going to be some

374
00:13:56,399 --> 00:14:01,839
more ACID math and another opportunity

375
00:13:59,040 --> 00:14:04,079
to integer overflow. So, basically, behind

376
00:14:01,839 --> 00:14:06,000
the scenes integer overflows even if you

377
00:14:04,079 --> 00:14:08,480
don't see it in your code itself it

378
00:14:06,000 --> 00:14:10,160
could be happening in the allocator.

379
00:14:08,480 --> 00:14:12,160
So, here's all of the different vendors,

380
00:14:10,160 --> 00:14:14,800
you know, that they were involved in this

381
00:14:12,160 --> 00:14:17,040
disclosure, you know, Amazon Free RTOS,

382
00:14:14,800 --> 00:14:20,720
Blackberry QNX,

383
00:14:17,040 --> 00:14:23,040
what else we got, Red Hat newlib, RIOT OS,

384
00:14:20,720 --> 00:14:25,519
all sorts of things, Texas Instruments,

385
00:14:23,040 --> 00:14:27,600
Windriver VxWorks, whole bunch of

386
00:14:25,519 --> 00:14:29,920
things have this sort of vulnerability

387
00:14:27,600 --> 00:14:31,600
and again from my experience I've seen

388
00:14:29,920 --> 00:14:33,279
this a lot in a lot of code as well

389
00:14:31,600 --> 00:14:35,600
anytime someone goes off and decides to

390
00:14:33,279 --> 00:14:37,519
make their own new custom allocator

391
00:14:35,600 --> 00:14:38,959
it leads to sort of,

392
00:14:37,519 --> 00:14:40,800
if an allocator had already been

393
00:14:38,959 --> 00:14:42,800
hardened against heap overflows, the new

394
00:14:40,800 --> 00:14:44,720
allocator tends to not be hardened it

395
00:14:42,800 --> 00:14:47,199
already been hardened against integer

396
00:14:44,720 --> 00:14:48,240
overflows it tends to not be hardened.

397
00:14:47,199 --> 00:14:50,160
So, this is something that you should

398
00:14:48,240 --> 00:14:52,160
definitely check into. And while we're on

399
00:14:50,160 --> 00:14:53,680
the topic of allocators, if you do go

400
00:14:52,160 --> 00:14:55,440
digging around in your allocator, I'd

401
00:14:53,680 --> 00:14:57,920
like to point out that you should

402
00:14:55,440 --> 00:15:00,160
"Just Say N0 to Zero". So, when you have these

403
00:14:57,920 --> 00:15:02,720
integer overflows, it turns out that some

404
00:15:00,160 --> 00:15:05,920
allocators are just happy to allocate a

405
00:15:02,720 --> 00:15:08,399
zero sized buffer, if an allocator hands

406
00:15:05,920 --> 00:15:10,720
you back a pointer to a zero size buffer

407
00:15:08,399 --> 00:15:12,320
you are guaranteed heap overflowing that

408
00:15:10,720 --> 00:15:14,880
buffer, right? If you ever try to write

409
00:15:12,320 --> 00:15:18,079
any data into it. So, when you're looking

410
00:15:14,880 --> 00:15:20,639
go look for zero size allocations being

411
00:15:18,079 --> 00:15:23,279
an acceptable thing and disallow it

412
00:15:20,639 --> 00:15:25,839
because integer overflows could lead to

413
00:15:23,279 --> 00:15:27,839
a value of zero and subsequently this is

414
00:15:25,839 --> 00:15:29,680
going to guarantee heap overflow.

415
00:15:27,839 --> 00:15:31,440
And here's the citations of that Microsoft

416
00:15:29,680 --> 00:15:32,880
work that'll be on the web page.

417
00:15:31,440 --> 00:15:34,560
All right? Well, we've talked a lot about

418
00:15:32,880 --> 00:15:36,480
integer overflows. Now, let's talk about

419
00:15:34,560 --> 00:15:38,800
integer underflows!

420
00:15:36,480 --> 00:15:41,040
What is it? Well, let's have our same dumb

421
00:15:38,800 --> 00:15:42,639
code that sets "i=0;" and now,

422
00:15:41,040 --> 00:15:44,880
infinitely,

423
00:15:42,639 --> 00:15:47,600
decrements the value and decreases it.

424
00:15:44,880 --> 00:15:49,680
Well, you started at zero and, you know,

425
00:15:47,600 --> 00:15:52,240
you can't really subtract one from zero.

426
00:15:49,680 --> 00:15:56,399
So, you need to borrow a one, and so, ones

427
00:15:52,240 --> 00:15:58,399
are zero minus one is all ones and that

428
00:15:56,399 --> 00:16:00,720
means you loop back around from being

429
00:15:58,399 --> 00:16:03,839
zero up to being the maximum possible

430
00:16:00,720 --> 00:16:06,240
value. That is an integer underflow!

431
00:16:03,839 --> 00:16:07,279
"BOOM". Integer underflow!

432
00:16:06,240 --> 00:16:08,480
All right, and you just keep decrementing,

433
00:16:07,279 --> 00:16:11,279
decrementing,

434
00:16:08,480 --> 00:16:13,120
decrementing, and once again loop around

435
00:16:11,279 --> 00:16:15,920
integer underflow.

436
00:16:13,120 --> 00:16:19,600
In the context of signed values, same thing.

437
00:16:15,920 --> 00:16:21,839
Loop around, "BOOM"! Integer underflow!

438
00:16:19,600 --> 00:16:23,839
And decrementing, decrementing, and once

439
00:16:21,839 --> 00:16:25,759
you cross this negative to positive

440
00:16:23,839 --> 00:16:28,880
boundary,

441
00:16:25,759 --> 00:16:30,480
"BOOM"! Integer underflow!

442
00:16:28,880 --> 00:16:32,160
And of course you just keep decrementing

443
00:16:30,480 --> 00:16:34,639
it again it's an integer underflow once

444
00:16:32,160 --> 00:16:36,720
you cross over from zero back to

445
00:16:34,639 --> 00:16:38,800
negative values, right? So,

446
00:16:36,720 --> 00:16:40,800
integer underflows, you know, you're moving down

447
00:16:38,800 --> 00:16:43,040
in values and you're crossing, you know,

448
00:16:40,800 --> 00:16:45,040
from positive values or zero values into

449
00:16:43,040 --> 00:16:47,600
negative or crossing from negative into

450
00:16:45,040 --> 00:16:49,519
positive, that is an integer underflow.

451
00:16:47,600 --> 00:16:51,600
So, how can an integer underflow lead to a

452
00:16:49,519 --> 00:16:54,240
security vulnerability? Well, the

453
00:16:51,600 --> 00:16:57,120
underflow leads to a value going from

454
00:16:54,240 --> 00:16:59,680
being small to being big. So, if that

455
00:16:57,120 --> 00:17:02,800
value is used, for instance, as a size to

456
00:16:59,680 --> 00:17:04,880
copy, then that could lead to an "over-copy".

457
00:17:02,800 --> 00:17:06,959
So, for instance, if an attacker controls

458
00:17:04,880 --> 00:17:08,880
this value and it's being subtracted

459
00:17:06,959 --> 00:17:10,480
from some other value if they can just

460
00:17:08,880 --> 00:17:11,839
set it to whatever they want, then they

461
00:17:10,480 --> 00:17:13,919
can make sure that it's bigger than the

462
00:17:11,839 --> 00:17:16,640
small value and that will, subsequently,

463
00:17:13,919 --> 00:17:18,400
underflow leading to a large size, right?

464
00:17:16,640 --> 00:17:20,160
That's ACID math!

465
00:17:18,400 --> 00:17:22,720
On the other hand, if the attacker

466
00:17:20,160 --> 00:17:25,600
controls this value and there's some,

467
00:17:22,720 --> 00:17:27,520
you know, constant or some other value being

468
00:17:25,600 --> 00:17:29,679
subtracted from the attacker controlled

469
00:17:27,520 --> 00:17:31,840
value, then they just need to set this to

470
00:17:29,679 --> 00:17:34,080
being smaller than that and, subsequently,

471
00:17:31,840 --> 00:17:36,559
it will once again underflow, leading to

472
00:17:34,080 --> 00:17:38,960
a large attacker controlled size value.

473
00:17:36,559 --> 00:17:40,880
And, if that size is used in a "memcpy",

474
00:17:38,960 --> 00:17:42,640
that's going to be an "over-copy".

475
00:17:40,880 --> 00:17:45,200
So, let's go look at some real examples

476
00:17:42,640 --> 00:17:45,200
of this now.

