1
00:00:00,160 --> 00:00:04,799
now let's talk about out-of-bound write

2
00:00:02,080 --> 00:00:08,080
vulnerabilities what is it well it's

3
00:00:04,799 --> 00:00:10,559
when ACID is used to calculate a pointer

4
00:00:08,080 --> 00:00:12,240
to some memory which is out of bounds of

5
00:00:10,559 --> 00:00:14,719
the intended location

6
00:00:12,240 --> 00:00:16,720
and subsequently ACID data is written to

7
00:00:14,719 --> 00:00:19,039
that memory location

8
00:00:16,720 --> 00:00:20,880
now this is equally applicable to

9
00:00:19,039 --> 00:00:23,600
whether or not you're thinking about the

10
00:00:20,880 --> 00:00:26,880
intended memory locations as buffers on

11
00:00:23,600 --> 00:00:28,240
the stack heap global memory or whether

12
00:00:26,880 --> 00:00:30,480
there's something like a custom

13
00:00:28,240 --> 00:00:32,320
allocation done through some api

14
00:00:30,480 --> 00:00:35,120
the reason that we differentiate these

15
00:00:32,320 --> 00:00:36,800
from linear buffer overflows like stack

16
00:00:35,120 --> 00:00:39,840
overflows heap overflows and global

17
00:00:36,800 --> 00:00:41,280
overflows is because while it's true

18
00:00:39,840 --> 00:00:43,360
that all of those linear buffer

19
00:00:41,280 --> 00:00:45,760
overflows are out of bound write

20
00:00:43,360 --> 00:00:47,520
vulnerabilities not all out-of-bound

21
00:00:45,760 --> 00:00:49,680
write vulnerabilities are linear buffer

22
00:00:47,520 --> 00:00:51,199
overflows for the common causes of

23
00:00:49,680 --> 00:00:53,199
out-of-bound writes. I'm going to make a

24
00:00:51,199 --> 00:00:55,039
distinction between intrinsic

25
00:00:53,199 --> 00:00:57,440
vulnerabilities which are found just

26
00:00:55,039 --> 00:00:58,960
directly inside of the source code and

27
00:00:57,440 --> 00:01:01,039
manufactured vulnerabilities where an

28
00:00:58,960 --> 00:01:03,600
attacker achieves out-of-bound writes

29
00:01:01,039 --> 00:01:05,840
but not directly via problems in the

30
00:01:03,600 --> 00:01:07,680
code so with intrinsic vulnerabilities

31
00:01:05,840 --> 00:01:11,040
these are generally down to pointer

32
00:01:07,680 --> 00:01:13,200
arithmetic gone awry for instance using

33
00:01:11,040 --> 00:01:15,840
ACID value as an array index if you're

34
00:01:13,200 --> 00:01:17,360
dealing with a proper array buffer

35
00:01:15,840 --> 00:01:18,880
on the other hand if you have something

36
00:01:17,360 --> 00:01:20,799
that's a little more complicated in

37
00:01:18,880 --> 00:01:23,439
terms of data structure than just a

38
00:01:20,799 --> 00:01:26,240
linear array then you might be accessing

39
00:01:23,439 --> 00:01:28,159
it via a base plus offset mechanism but

40
00:01:26,240 --> 00:01:30,159
if that offset is ultimately attack or

41
00:01:28,159 --> 00:01:32,560
controlled then that can allow them to

42
00:01:30,159 --> 00:01:34,560
sort of skip forward through the

43
00:01:32,560 --> 00:01:37,280
intended memory locations or even skip

44
00:01:34,560 --> 00:01:39,759
backwards and ultimately land outside of

45
00:01:37,280 --> 00:01:42,159
the bounds of the buffer and cause a

46
00:01:39,759 --> 00:01:44,320
write to occur at that point

47
00:01:42,159 --> 00:01:46,000
making it a little bit more source cody

48
00:01:44,320 --> 00:01:48,720
you know, you can think of array with

49
00:01:46,000 --> 00:01:50,720
ACID inside equal to ACID so if some

50
00:01:48,720 --> 00:01:52,799
ACID is assigned into an array where the

51
00:01:50,720 --> 00:01:54,799
index is attack controlled that's an out

52
00:01:52,799 --> 00:01:56,719
of bound write now of course we know

53
00:01:54,799 --> 00:01:58,719
behind the scenes even this sort of

54
00:01:56,719 --> 00:02:00,799
construction in C is really just point

55
00:01:58,719 --> 00:02:02,799
arithmetic so it might also occur that

56
00:02:00,799 --> 00:02:04,960
you have base plus ACID it goes into a

57
00:02:02,799 --> 00:02:06,719
pointer and then ACID goes into the

58
00:02:04,960 --> 00:02:08,879
dereference pointer and that's an outer

59
00:02:06,719 --> 00:02:11,360
bond write and in the extreme case of

60
00:02:08,879 --> 00:02:14,080
doing something wrong the code may just

61
00:02:11,360 --> 00:02:16,480
straight up take a pointer that is ACID

62
00:02:14,080 --> 00:02:18,239
from the attacker and use it to write

63
00:02:16,480 --> 00:02:20,080
into memory we'll see an example of that

64
00:02:18,239 --> 00:02:20,840
in this section

65
00:02:20,080 --> 00:02:22,640
now

66
00:02:20,840 --> 00:02:24,959
manufactured out-of-bound write

67
00:02:22,640 --> 00:02:27,040
vulnerabilities are things where the

68
00:02:24,959 --> 00:02:28,959
out-of-bound write is not directly one

69
00:02:27,040 --> 00:02:31,280
of those causes that I was just showing

70
00:02:28,959 --> 00:02:33,040
but instead the attacker utilizes

71
00:02:31,280 --> 00:02:34,720
something like a linear buffer overflow

72
00:02:33,040 --> 00:02:37,680
such as a stack overflow or heap

73
00:02:34,720 --> 00:02:39,680
overflow and what they corrupt is maybe

74
00:02:37,680 --> 00:02:41,360
not directly the

75
00:02:39,680 --> 00:02:43,280
return address on the stack for instance

76
00:02:41,360 --> 00:02:45,519
maybe they there's a stack canary and

77
00:02:43,280 --> 00:02:47,200
they've got a pure linear overflow and

78
00:02:45,519 --> 00:02:48,959
if they, you know, just tried to hit the

79
00:02:47,200 --> 00:02:50,480
return address they would hit the stack

80
00:02:48,959 --> 00:02:52,720
canary and they don't have any other way

81
00:02:50,480 --> 00:02:56,000
around it so that's maybe not a viable

82
00:02:52,720 --> 00:02:58,239
path for them if instead they corrupt a

83
00:02:56,000 --> 00:02:59,920
adjacent local variable that happens to

84
00:02:58,239 --> 00:03:02,400
be a pointer and if they can

85
00:02:59,920 --> 00:03:03,440
subsequently cause ACID to be written to

86
00:03:02,400 --> 00:03:06,400
that now

87
00:03:03,440 --> 00:03:07,840
overwritten ACID pointer then that would

88
00:03:06,400 --> 00:03:09,040
constitute an out of bound write

89
00:03:07,840 --> 00:03:11,519
vulnerability

90
00:03:09,040 --> 00:03:13,519
and so the important thing here is that

91
00:03:11,519 --> 00:03:16,159
once that sort of construction occurs

92
00:03:13,519 --> 00:03:19,120
that is what we call an arbitrary write

93
00:03:16,159 --> 00:03:21,920
or a write-what-where primitive and this

94
00:03:19,120 --> 00:03:24,000
is absolutely one of the most strong

95
00:03:21,920 --> 00:03:25,440
primitives that an attacker can have

96
00:03:24,000 --> 00:03:27,280
essentially you can think if they can

97
00:03:25,440 --> 00:03:28,720
write anywhere they want inside of the

98
00:03:27,280 --> 00:03:30,959
memory range there's a lot of

99
00:03:28,720 --> 00:03:33,200
possibilities for what they can do to

100
00:03:30,959 --> 00:03:34,799
take over the program. So, again, why do we

101
00:03:33,200 --> 00:03:36,879
differentiate between out-of-bound

102
00:03:34,799 --> 00:03:38,799
writes and linear buffer overflows? It's

103
00:03:36,879 --> 00:03:40,879
because, you know, some vulnerabilities

104
00:03:38,799 --> 00:03:42,799
don't directly lend themselves to

105
00:03:40,879 --> 00:03:45,360
categorization as a linear buffer

106
00:03:42,799 --> 00:03:47,760
overflow and once you have things like

107
00:03:45,360 --> 00:03:49,920
manufactured out of bound writes then

108
00:03:47,760 --> 00:03:52,319
this becomes a much stronger primitive a

109
00:03:49,920 --> 00:03:54,959
mechanism for bypassing

110
00:03:52,319 --> 00:03:56,640
things like exploit mitigations and,

111
00:03:54,959 --> 00:03:58,319
you know, consequently this just puts a lot

112
00:03:56,640 --> 00:04:00,560
of power in the attacker's hands. So I

113
00:03:58,319 --> 00:04:03,120
just want to use this to re-emphasize

114
00:04:00,560 --> 00:04:05,920
that a manufactured out-of-bound write

115
00:04:03,120 --> 00:04:08,720
starts as some other vulnerability class

116
00:04:05,920 --> 00:04:11,040
so it starts as a heap overflow a

117
00:04:08,720 --> 00:04:13,120
linear stack overflow etc that's why

118
00:04:11,040 --> 00:04:15,439
it's important to close down those other

119
00:04:13,120 --> 00:04:16,959
primitives even if, you know, it doesn't

120
00:04:15,439 --> 00:04:18,479
seem, you know, even if you think, you know,

121
00:04:16,959 --> 00:04:20,479
the mitigation will save me I've got a

122
00:04:18,479 --> 00:04:22,960
stack canary. Well, again, you know, it all

123
00:04:20,479 --> 00:04:26,479
depends extremely much on the very

124
00:04:22,960 --> 00:04:28,800
specific nature of what data is adjacent

125
00:04:26,479 --> 00:04:30,800
uh to a linear buffer overflow. So, by

126
00:04:28,800 --> 00:04:33,199
closing down the sort of weaker

127
00:04:30,800 --> 00:04:35,360
primitive of a linear buffer overflow

128
00:04:33,199 --> 00:04:37,440
then you can in some cases close down

129
00:04:35,360 --> 00:04:40,000
these manufactured out of bound writes

130
00:04:37,440 --> 00:04:42,160
these manufactured write-what-wheres that

131
00:04:40,000 --> 00:04:43,759
would otherwise cause an extremely

132
00:04:42,160 --> 00:04:45,120
powerful primitive to come into the

133
00:04:43,759 --> 00:04:47,199
attacker's hands.

134
00:04:45,120 --> 00:04:48,560
All right, so trivial example out of

135
00:04:47,199 --> 00:04:50,080
bound writes we've got, you know, the

136
00:04:48,560 --> 00:04:51,600
trivialist of trivial example we

137
00:04:50,080 --> 00:04:54,160
literally are just going to give the

138
00:04:51,600 --> 00:04:55,840
attacker control over I and j and then

139
00:04:54,160 --> 00:04:58,240
write attacker controlled value in here

140
00:04:55,840 --> 00:05:01,039
so buffer is eight bytes

141
00:04:58,240 --> 00:05:03,440
string two unsigned long long taking in

142
00:05:01,039 --> 00:05:06,880
from attacker control to "argv"

143
00:05:03,440 --> 00:05:08,880
into "i", attacker controlled "arv[2]" into "j"

144
00:05:06,880 --> 00:05:10,479
and so write attacker control data to a

145
00:05:08,880 --> 00:05:12,160
location of their choosing. All right, so

146
00:05:10,479 --> 00:05:14,800
if we run our trivial example, for

147
00:05:12,160 --> 00:05:16,720
instance, with one one, "argv[1]" was

148
00:05:14,800 --> 00:05:18,800
the value being written "argv[2]" is

149
00:05:16,720 --> 00:05:21,199
the offset to write it to so one is

150
00:05:18,800 --> 00:05:22,880
written into a buffer of one well no

151
00:05:21,199 --> 00:05:26,080
problem there no crash

152
00:05:22,880 --> 00:05:27,919
if we say out of bound write at offset 8

153
00:05:26,080 --> 00:05:30,639
which again this is a 8 byte buffer

154
00:05:27,919 --> 00:05:33,199
which means index 8 is out of bounds and

155
00:05:30,639 --> 00:05:35,680
we're writing 41 4141 which should look

156
00:05:33,199 --> 00:05:39,120
magical to you should look like an ACID

157
00:05:35,680 --> 00:05:41,039
burn because that is the hexadecimal

158
00:05:39,120 --> 00:05:43,520
value for "A"s

159
00:05:41,039 --> 00:05:45,280
so if I do that though it's actually no

160
00:05:43,520 --> 00:05:46,960
crash so I've just written out a balance

161
00:05:45,280 --> 00:05:49,039
and there's no crash well that's

162
00:05:46,960 --> 00:05:52,000
interesting if I do another one and i

163
00:05:49,039 --> 00:05:54,000
say now index nine well this does cause

164
00:05:52,000 --> 00:05:56,080
a crash and specifically it says

165
00:05:54,000 --> 00:05:58,080
"*** stack smashing detected ***" so perhaps there's

166
00:05:56,080 --> 00:05:59,919
some mitigation in play there if I write

167
00:05:58,080 --> 00:06:02,720
it offset 10

168
00:05:59,919 --> 00:06:05,520
well that does also not cause a crash

169
00:06:02,720 --> 00:06:07,840
and offset 11 does cause a crash again

170
00:06:05,520 --> 00:06:09,280
so just our observed behavior here even

171
00:06:07,840 --> 00:06:10,720
if we didn't, you know, know exactly what

172
00:06:09,280 --> 00:06:12,720
the stack looks like I'm just going to

173
00:06:10,720 --> 00:06:15,600
tell you that, you know, "i" and "j" are right

174
00:06:12,720 --> 00:06:19,120
here and "buf" is here and so we write

175
00:06:15,600 --> 00:06:21,120
4141 to "buf[8]" which is outside

176
00:06:19,120 --> 00:06:23,360
of the bounds of the thing and nothing

177
00:06:21,120 --> 00:06:25,280
happens no crash

178
00:06:23,360 --> 00:06:27,440
not exactly clear what's going on there

179
00:06:25,280 --> 00:06:28,639
if we write to offset nine then we do

180
00:06:27,440 --> 00:06:31,199
get a crash

181
00:06:28,639 --> 00:06:34,400
offset ten nothing happens

182
00:06:31,199 --> 00:06:36,160
offset eleven gotta crash again so if

183
00:06:34,400 --> 00:06:38,479
you would take one of our classes at

184
00:06:36,160 --> 00:06:39,759
OST2 like the assembly class you could

185
00:06:38,479 --> 00:06:41,840
go and, you know, throw this into a

186
00:06:39,759 --> 00:06:43,840
debugger yourself and reconstruct the

187
00:06:41,840 --> 00:06:46,639
stack diagram but I can tell you from

188
00:06:43,840 --> 00:06:49,120
having done that myself that this offset

189
00:06:46,639 --> 00:06:50,639
is unused padding space and therefore,

190
00:06:49,120 --> 00:06:53,360
you know, it's just used to align the

191
00:06:50,639 --> 00:06:55,360
stack and it's therefore not a problem

192
00:06:53,360 --> 00:06:58,080
if that gets overwritten

193
00:06:55,360 --> 00:06:59,599
this offset instead is the stack canary

194
00:06:58,080 --> 00:07:02,240
this is the exploit mitigation and

195
00:06:59,599 --> 00:07:04,160
that's why when we wrote to offset nine

196
00:07:02,240 --> 00:07:06,800
it specifically said stack smashing

197
00:07:04,160 --> 00:07:09,039
detected because the check

198
00:07:06,800 --> 00:07:11,120
for overflows that occurs before a

199
00:07:09,039 --> 00:07:13,520
function returns before main returns in

200
00:07:11,120 --> 00:07:15,280
this case the check is saying oh hey i

201
00:07:13,520 --> 00:07:16,880
can see my canary has been over it and

202
00:07:15,280 --> 00:07:18,080
looks like there's stack smashing going

203
00:07:16,880 --> 00:07:20,160
on.

204
00:07:18,080 --> 00:07:21,840
This, right here, was the saved

205
00:07:20,160 --> 00:07:24,319
RBP register which, you know, you don't really

206
00:07:21,840 --> 00:07:25,919
need to care about and this is the saved

207
00:07:24,319 --> 00:07:28,639
return address which you do need to care

208
00:07:25,919 --> 00:07:31,039
about because we said that this is the

209
00:07:28,639 --> 00:07:32,720
typical target of

210
00:07:31,039 --> 00:07:33,919
target of exploitation now I said you

211
00:07:32,720 --> 00:07:35,759
don't need to care about this I only

212
00:07:33,919 --> 00:07:37,360
mean in the sense of, you know, we're not

213
00:07:35,759 --> 00:07:40,080
assuming that you know assembly in this

214
00:07:37,360 --> 00:07:42,160
class there have been exploits where

215
00:07:40,080 --> 00:07:44,960
literally a single byte overwritten into

216
00:07:42,160 --> 00:07:46,800
a saved RBP register will allow the

217
00:07:44,960 --> 00:07:48,879
attacker to take over

218
00:07:46,800 --> 00:07:50,800
the control of the execution so

219
00:07:48,879 --> 00:07:52,000
overwrites anywhere are bad! I'm just

220
00:07:50,800 --> 00:07:53,440
saying, you know, you don't need to care

221
00:07:52,000 --> 00:07:55,360
about that for now because I can't

222
00:07:53,440 --> 00:07:58,960
assume you know assembly. And, with that,

223
00:07:55,360 --> 00:07:58,960
let's go look at some real examples.

